import { Cancion } from './cancion';
export class ListaDeReproduccion{
    nombre:string;
    descripcion:string;
    caciones : Cancion[];

    constructor(){
        this.caciones = new Array<Cancion>();
    }
}