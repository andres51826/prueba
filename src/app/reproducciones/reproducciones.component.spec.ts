import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReproduccionesComponent } from './reproducciones.component';

describe('ReproduccionesComponent', () => {
  let component: ReproduccionesComponent;
  let fixture: ComponentFixture<ReproduccionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReproduccionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReproduccionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
