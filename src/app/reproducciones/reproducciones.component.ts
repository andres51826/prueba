import { ListaDeReproduccion } from './../models/listaDeReproduccion';
import { getTestBed } from '@angular/core/testing';
import { ReproduccionesService } from './../services/reproducciones.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reproducciones',
  templateUrl: './reproducciones.component.html',
  styleUrls: ['./reproducciones.component.css']
})
export class ReproduccionesComponent implements OnInit {

  constructor(public reproduccionesService : ReproduccionesService) { }
  reproducciones : ListaDeReproduccion[];
  habilitarTabla : boolean = true;
  habilitarFormulario : boolean = false;
  listaDeReproduccion : ListaDeReproduccion = new ListaDeReproduccion();
  error: string[];

  ngOnInit(): void {

    this.reproduccionesService.getListReproducciones().subscribe(
      reproducciones => this.reproducciones = reproducciones
    )
    
  }

  crearNuevaListaDeReproducciones(){
    this.habilitarTabla = false;
    this.habilitarFormulario = true;
  }

  volver(){
    this.habilitarTabla = true;
    this.habilitarFormulario = false;
  }

  newListaReproduccion(){
    console.log(this.listaDeReproduccion);
    this.reproduccionesService.crearListaDeReproduccion(this.listaDeReproduccion).subscribe(
      json => {
        Swal.fire('Lista De Reproducciones','La lista se creo correctamente', 'success');
        this.reproducciones.push(json);
        console.log(json);
      },
      err => {
        this.error = err.error as string[];
        console.error('Código del error desde el backend: ' + err.status);
        Swal.fire('Error', `${err.error.mensaje}`, 'error');
        console.error(err.error);
      }
    )
  }

  eliminarLista(lista :ListaDeReproduccion){

    Swal.fire({
      title: '¿Esta seguro de eliminar La lista de reproducción?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonColor : '#8F141B',
      cancelButtonText: 'No'
    }).then((result) => {
      console.log(result)
      if(result.value){
        this.reproduccionesService.eliminarListaDeReproduccion(lista).subscribe(
          json => {
            Swal.fire('Eliminado','Se elimino la lista de reproducción correctamente.', 'success');
            console.log(json)
            this.reproduccionesService.getListReproducciones().subscribe(
              reproducciones => this.reproducciones = reproducciones
            )
          },
          err => {
            this.error = err.error as string[];
            console.error('Código del error desde el backend: ' + err.status);
            Swal.fire('Error', `${err.error.mensaje}`, 'error');
            console.error(err.error);
          }
        )
      }
    });
  }

}
