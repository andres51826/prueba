import { ListaDeReproduccion } from './../models/listaDeReproduccion';
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError , map} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ReproduccionesService {
  private url:string = 'http://localhost:8080';
  private httpHeaders = new HttpHeaders({'Content-type':'application/json'})

  constructor(private http:HttpClient,private router:Router) { }

  getListReproducciones():Observable<ListaDeReproduccion[]>{
    return this.http.get<ListaDeReproduccion[]>(this.url + "/lists").pipe(
      catchError(e=>{
        return throwError(e);
      })
    )
  }


  eliminarListaDeReproduccion(lista : ListaDeReproduccion):Observable<any>{
    return this.http.delete<any>(this.url + "/lists/"+lista.nombre).pipe(
      catchError(e=>{
        return throwError(e);
      })
    )
  }

  crearListaDeReproduccion(lista:ListaDeReproduccion): Observable<any>{
    return this.http.post(this.url+"/lists", lista ,{headers : this.httpHeaders})
    .pipe(
      map((response: any) => response as any),
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }));
  }

}
