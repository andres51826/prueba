import { TestBed } from '@angular/core/testing';

import { ReproduccionesService } from './reproducciones.service';

describe('ReproduccionesService', () => {
  let service: ReproduccionesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReproduccionesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
