import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ReproduccionesComponent } from './reproducciones/reproducciones.component';

import { RouterModule ,Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

const routes : Routes = [
  {path: '', redirectTo: 'reproducciones',pathMatch: 'full'},
  {path: 'reproducciones', component:ReproduccionesComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    ReproduccionesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    CommonModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
